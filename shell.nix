{ sources ? import ./nix/sources.nix
, pkgs ? import sources.nixpkgs {}
, ghc ? pkgs.haskellPackages.ghcWithPackages (pkgs: with pkgs; [
    safe
    dir-traverse
  ])
}:

pkgs.mkShell {
  buildInputs = [
    ghc
    pkgs.exa
  ];

  shellHook = ''
    echo this is a nix shell; alias l="ls -al"
  '';

  MY_ENVIRONMENT_VARIABLE = "world";
}
