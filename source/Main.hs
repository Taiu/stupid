module Main where

import System.Directory
import System.Directory.Recursive
import Data.List
import qualified Data.Map.Strict as Map

import Debug.Trace

nameOfFolderOfLinks = "stupid"
nameOfStupidLink = "stupid_link"

allowedNormalCharacters = ""
  ++ "_-"
  ++ "abcdefghijklmnopqrstuvwxyz"
  ++ "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

backslash = "/"

main = do
  homeDirectory_Raw <- getHomeDirectory
  let homeDirectory = homeDirectory_Raw ++ "/"

  files <- getDirFiltered filterForNotHavingASpaceInFilename homeDirectory

  let stupidLinks = filter (isSuffixOf nameOfStupidLink) files

  let outputLinks = prepare stupidLinks
  let size = Map.size outputLinks

  let locationOfLinkCollection = homeDirectory ++ nameOfFolderOfLinks ++ "/"
  storeExists <- doesDirectoryExist locationOfLinkCollection
  if storeExists
     then return ()
     else putStrLn $ "crating folder: " ++ locationOfLinkCollection

  let create_its_parents_too = False in createDirectoryIfMissing create_its_parents_too locationOfLinkCollection

  createLinks locationOfLinkCollection outputLinks

  putStrLn $ "finished creating links, in total: " ++ show (size)

filterForNotHavingASpaceInFilename directory = do
  let doesnotContainSpaces = not $ elem ' ' directory
  let onlyContainsGoodCharacters = containsOnlyGoodBehavingChars directory
  return $ True
    && doesnotContainSpaces
    && onlyContainsGoodCharacters

containsOnlyGoodBehavingChars = and . map isGoodDirectoryCharactor

isGoodDirectoryCharactor x = elem x $ backslash ++ allowedNormalCharacters
isGoodFilenameCharactor x = elem x $ allowedNormalCharacters

prepare links = result
  where
    result = linksAsMap

    locationsAndNames = map locationAndName links

    linksAsMap = Map.fromListWithKey inCaseOfNonUniqueness locationsAndNames

    inCaseOfNonUniqueness key firstLocation secondLocation = error $ ""
      ++ "There are two "
      ++ nameOfStupidLink
      ++ " files that are within an folder that has the same name: "
      ++ show (key, firstLocation, secondLocation)

dropTail n = reverse . drop n . reverse

dropTailWhile f = reverse . takeWhile f . reverse

locationAndName link = result
  where
    result = (linkName, magic_Filename_removed)

    magic_Filename_removed = dropTail (length nameOfStupidLink) link

    removeTrailingSlash = dropTail 1 magic_Filename_removed

    linkName = dropTailWhile isGoodFilenameCharactor removeTrailingSlash
    restPath = dropTail (length linkName) removeTrailingSlash

createLinks locationOfLinkCollection links
  = id
  $ mapM_ (createLink locationOfLinkCollection)
  $ Map.toList links

createLink locationOfLinkCollection (link_name, pointingTo) = do
  let linkLocation = locationOfLinkCollection ++ link_name

  putStrLn $ ""
    ++ "creating link: "
    ++ linkLocation
    ++ ", pointing to: "
    ++ pointingTo

  writeFile linkLocation pointingTo
